package com.imb.a20220525_mirzokhidjonikromov_nycschools.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.imb.a20220525_mirzokhidjonikromov_nycschools.R
import com.imb.a20220525_mirzokhidjonikromov_nycschools.databinding.ItemLoadingBinding
import com.imb.a20220525_mirzokhidjonikromov_nycschools.databinding.SchoolsItemViewBinding
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.School
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.gone
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.visible

class SchoolsListAdapter :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val schools: ArrayList<School> = ArrayList()

    companion object {
        var ADAPTER_STATE_OVER = 0
        var ADAPTER_STATE_LOADING = 1
        var ADAPTER_STATE_ERROR = 2
    }

    private var adapterState = ADAPTER_STATE_LOADING

    private lateinit var listener: (String, Int) -> Unit

    fun setOnDetailsBtnClickListener(listener: (String, Int) -> Unit) {
        this.listener = listener
    }

    fun setAdapterState(state: Int) {
        adapterState = state
        notifyDataSetChanged()
    }

    fun getAdapterState() = adapterState

    fun clearData() {
        schools.clear()
        adapterState = ADAPTER_STATE_LOADING
        notifyDataSetChanged()
    }

    fun setSchoolsList(data: List<School>) {
        schools.clear()
        schools.addAll(data)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> DataViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.schools_item_view, parent, false
                )
            )
            else -> LoadingViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_loading, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == schools.size) {
            when (adapterState) {
                ADAPTER_STATE_ERROR, ADAPTER_STATE_LOADING -> {
                    (holder as LoadingViewHolder).bind(adapterState)
                }
            }
            return
        } else
            (holder as DataViewHolder).bind(schools[position])
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == schools.size)
            1
        else
            0
    }

    override fun getItemCount(): Int {
        return if (adapterState == ADAPTER_STATE_OVER) schools.size
        else schools.size + 1
    }

    inner class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = SchoolsItemViewBinding.bind(itemView)

        fun bind(school: School) {
            binding.schoolName.text = school.schoolName
            binding.schoolInfo.text = school.overviewParagraph
            binding.detailsBtn.setOnClickListener {
                listener.invoke(school.dbn ?: "", adapterPosition)
            }
        }
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemLoadingBinding.bind(itemView)
        fun bind(adapterState: Int) {
            itemView.apply {
                binding.layout.setBackgroundResource(R.color.gray)
                when (adapterState) {
                    ADAPTER_STATE_OVER -> binding.progress.gone()
                    ADAPTER_STATE_LOADING -> binding.progress.visible()
                    ADAPTER_STATE_ERROR -> binding.progress.gone()
                }
            }
        }
    }
}