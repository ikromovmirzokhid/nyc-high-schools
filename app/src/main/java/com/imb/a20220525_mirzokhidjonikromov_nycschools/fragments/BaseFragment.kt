package com.imb.a20220525_mirzokhidjonikromov_nycschools.fragments

import androidx.fragment.app.Fragment
import com.imb.a20220525_mirzokhidjonikromov_nycschools.MainActivity

open class BaseFragment(layoutId: Int) : Fragment(layoutId) {

    fun getMain(): MainActivity? {
        return requireActivity() as MainActivity
    }
}