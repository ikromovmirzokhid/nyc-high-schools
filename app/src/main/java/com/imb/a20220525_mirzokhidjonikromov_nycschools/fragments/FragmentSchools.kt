package com.imb.a20220525_mirzokhidjonikromov_nycschools.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.imb.a20220525_mirzokhidjonikromov_nycschools.R
import com.imb.a20220525_mirzokhidjonikromov_nycschools.adapters.SchoolsListAdapter
import com.imb.a20220525_mirzokhidjonikromov_nycschools.adapters.SchoolsListAdapter.Companion.ADAPTER_STATE_ERROR
import com.imb.a20220525_mirzokhidjonikromov_nycschools.adapters.SchoolsListAdapter.Companion.ADAPTER_STATE_LOADING
import com.imb.a20220525_mirzokhidjonikromov_nycschools.adapters.SchoolsListAdapter.Companion.ADAPTER_STATE_OVER
import com.imb.a20220525_mirzokhidjonikromov_nycschools.databinding.FragmentSchoolsBinding
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component.MainComponent
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.School
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.Constants.DATA_NOT_FOUND
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.progressOff
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.progressOn
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.showError
import com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel.SchoolViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class FragmentSchools : BaseFragment(R.layout.fragment_schools) {
    private lateinit var binding: FragmentSchoolsBinding

    private val component by lazy {
        MainComponent.create()
    }

    private lateinit var schoolViewModel: SchoolViewModel

    private var totalSchoolItems = ArrayList<School>()
    private var currentSchoolsItems = ArrayList<School>()
    private lateinit var adapter: SchoolsListAdapter
    private lateinit var layoutManager: LinearLayoutManager

    private var currentItemsNum = 20
    private var loading = false
    private var initialLoad = false

    private var lastClickedPosition = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)

        if (!isDetached) {
            schoolViewModel = component.viewModelProvider().create(SchoolViewModel::class.java)
        }

        binding = FragmentSchoolsBinding.bind(view)


        initViews()
        if (!initialLoad) {
            initValues()
            loadDataFromNetwork()
        }
    }

    private fun initValues() {
        currentItemsNum = 20
        loading = false
    }

    private fun initViews() {
        adapter = SchoolsListAdapter()
        adapter.setSchoolsList(currentSchoolsItems)
        layoutManager = LinearLayoutManager(requireContext())
        binding.schoolsRv.layoutManager = layoutManager
        binding.schoolsRv.adapter = adapter

        if (lastClickedPosition != 0)
            binding.schoolsRv.scrollToPosition(lastClickedPosition)

        manageScroll()
        initListeners()
    }

    private fun loadDataFromNetwork() {
        progressOn()
        schoolViewModel.getAllSchools().observe(viewLifecycleOwner) {
            progressOff()
            if (it.data != null) {
                initialLoad = true
                totalSchoolItems.addAll(it.data)
                currentSchoolsItems.addAll(totalSchoolItems.subList(0, currentItemsNum))
                adapter.setSchoolsList(currentSchoolsItems)
                currentItemsNum += 20
            } else {
                showError(it.errorMessage ?: "")
            }
        }
    }

    private fun initListeners() {
        adapter.setOnDetailsBtnClickListener { dbn, position ->
            lastClickedPosition = position
            progressOn()
            schoolViewModel.getSchoolInfo(dbn).observe(viewLifecycleOwner) {
                progressOff()
                if (it.data != null) {
                    findNavController().navigate(
                        R.id.action_fragmentSchools_to_fragmentSchoolInfo,
                        bundleOf("schoolInfo" to Gson().toJson(it.data))
                    )
                } else {
                    if (it.errorMessage == DATA_NOT_FOUND) {
                        Snackbar.make(requireView(), R.string.data_not_found, Snackbar.LENGTH_SHORT)
                            .show()
                    } else
                        showError(it.errorMessage ?: "")
                }
            }
        }
    }

    private fun manageScroll() {
        binding.schoolsRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (layoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount - 1) {
                    if (currentItemsNum < totalSchoolItems.size) {
                        if (adapter.getAdapterState() == ADAPTER_STATE_LOADING)
                            if (!loading)
                                processData()
                    } else if (adapter.getAdapterState() != ADAPTER_STATE_ERROR) {
                        adapter.setAdapterState(ADAPTER_STATE_OVER)
                    }
                }
            }
        })
    }

    private fun processData() {
        loading = true
        CoroutineScope(Main).launch {
            adapter.setAdapterState(ADAPTER_STATE_LOADING)
            delay(300)
            loading = false
            if (currentItemsNum > totalSchoolItems.size) {
                currentItemsNum = totalSchoolItems.size
                adapter.setAdapterState(ADAPTER_STATE_OVER)
            }
            currentSchoolsItems.addAll(
                totalSchoolItems.subList(
                    currentSchoolsItems.size - 1,
                    currentItemsNum
                )
            )
            adapter.setSchoolsList(currentSchoolsItems)
            currentItemsNum += 20
        }
    }

}