package com.imb.a20220525_mirzokhidjonikromov_nycschools.fragments

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.imb.a20220525_mirzokhidjonikromov_nycschools.R
import com.imb.a20220525_mirzokhidjonikromov_nycschools.databinding.FragmentSchoolInfoBinding
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component.MainComponent
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.SchoolInfo
import com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel.SchoolViewModel

@Suppress("DEPRECATION")
class FragmentSchoolInfo : BaseFragment(R.layout.fragment_school_info) {
    private lateinit var binding: FragmentSchoolInfoBinding

    private val component by lazy {
        MainComponent.create()
    }

    private lateinit var schoolViewModel: SchoolViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!isDetached) {
            schoolViewModel = component.viewModelProvider().create(SchoolViewModel::class.java)
        }

        val schoolInfoJson = arguments?.getString("schoolInfo")
        val schoolInfo = Gson().fromJson(schoolInfoJson, SchoolInfo::class.java)

        binding = FragmentSchoolInfoBinding.bind(view)
        initSchoolInfo(schoolInfo)

    }

    private fun initSchoolInfo(data: SchoolInfo) {
        binding.schoolName.text = data.schoolName
        binding.testTakersNum.text = data.numOfTestTakers
        binding.mathAvg.text = data.satMathAvgScore
        binding.writingAvg.text = data.satWritingAvgScore
        binding.readingAvg.text = data.satCriticalReadingAvgScore
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            getMain()?.onBackPressed()
            return true
        }
        return false
    }
}