package com.imb.a20220525_mirzokhidjonikromov_nycschools.utils

data class BaseResponse<T>(
    val data: T?,
    val errorMessage: String? = null
)