package com.imb.a20220525_mirzokhidjonikromov_nycschools.app

import androidx.multidex.MultiDexApplication
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.DI
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component.DaggerNetworkComponent

class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        initDI()
    }

    private fun initDI() {
        DI.networkComponent =
            DaggerNetworkComponent.create()
    }
}