package com.imb.a20220525_mirzokhidjonikromov_nycschools.model

import com.google.gson.annotations.SerializedName

data class School (
    @SerializedName("dbn")
    val dbn : String? = null,
    @SerializedName("school_name")
    val schoolName : String? = null,
    @SerializedName("overview_paragraph")
    val overviewParagraph : String? = null
)