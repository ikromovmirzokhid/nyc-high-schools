package com.imb.a20220525_mirzokhidjonikromov_nycschools

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate

class MainActivity : AppCompatActivity() {

    private lateinit var progressDialog: AlertDialog
    private lateinit var view: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        closeNightMode()
        initDialog()
    }

    private fun closeNightMode() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    private fun initDialog() {
        view = layoutInflater.inflate(R.layout.dialog_progress_view, null)
        progressDialog = AlertDialog.Builder(this)
            .setView(view)
            .setCancelable(false)
            .create().apply {
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
    }

    fun hideProgress() {
        if (this::progressDialog.isInitialized) {
            progressDialog.cancel()
        }
    }

    fun showProgress() {
        if (this::progressDialog.isInitialized) {
            progressDialog.show()
        }
    }

    override fun onDestroy() {
        hideProgress()
        super.onDestroy()
    }
}