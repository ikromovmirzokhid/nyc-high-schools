package com.imb.a20220525_mirzokhidjonikromov_nycschools.di.scope

import javax.inject.Scope

@Scope
annotation class ScreenScope()
