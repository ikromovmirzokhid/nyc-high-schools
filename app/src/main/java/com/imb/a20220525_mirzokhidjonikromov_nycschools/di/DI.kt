package com.imb.a20220525_mirzokhidjonikromov_nycschools.di

import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component.NetworkComponent

object DI {
    lateinit var networkComponent: NetworkComponent
        internal set
}