package com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component

import androidx.lifecycle.ViewModel
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.DI
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.scope.ScreenScope
import com.imb.a20220525_mirzokhidjonikromov_nycschools.network.SchoolApi
import com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel.SchoolViewModel
import com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel.ViewModelKey
import com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel.ViewModelProvider
import dagger.*
import dagger.multibindings.IntoMap

@Component(modules = [MainModule::class])
@ScreenScope
interface MainComponent {
    fun viewModelProvider(): ViewModelProvider

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun schoolAPi(api: SchoolApi): Builder

        fun build(): MainComponent
    }

    companion object {
        fun create() =
            DaggerMainComponent.builder().schoolAPi(DI.networkComponent.schoolApi()).build()
    }
}

@Module
abstract class MainModule {

    @Binds
    @IntoMap
    @ViewModelKey(SchoolViewModel::class)
    abstract fun schoolViewModel(viewModel: SchoolViewModel): ViewModel
}