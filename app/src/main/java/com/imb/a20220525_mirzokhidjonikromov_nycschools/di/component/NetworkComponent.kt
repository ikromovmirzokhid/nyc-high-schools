package com.imb.a20220525_mirzokhidjonikromov_nycschools.di.component

import com.imb.a20220525_mirzokhidjonikromov_nycschools.network.SchoolApi
import dagger.Component
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Component(modules = [NetworkModule::class])
@Singleton
interface NetworkComponent {

    fun schoolApi(): SchoolApi

}

@Module
object NetworkModule {

    const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    @JvmStatic
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .build()

    @JvmStatic
    @Singleton
    @Provides
    fun provideSchoolAPi(retrofit: Retrofit) = retrofit.create(SchoolApi::class.java)


}
