package com.imb.a20220525_mirzokhidjonikromov_nycschools.network

import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.School
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.SchoolInfo
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.BaseResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {

    @GET("s3k6-pzi2.json")
    suspend fun getAllSchools(): List<School>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolInfo(@Query("dbn") dbn: String): List<SchoolInfo>
}