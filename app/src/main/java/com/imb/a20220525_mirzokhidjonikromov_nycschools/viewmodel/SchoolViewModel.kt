package com.imb.a20220525_mirzokhidjonikromov_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imb.a20220525_mirzokhidjonikromov_nycschools.di.scope.ScreenScope
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.School
import com.imb.a20220525_mirzokhidjonikromov_nycschools.model.SchoolInfo
import com.imb.a20220525_mirzokhidjonikromov_nycschools.network.SchoolApi
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.BaseResponse
import com.imb.a20220525_mirzokhidjonikromov_nycschools.utils.Constants.DATA_NOT_FOUND
import kotlinx.coroutines.launch
import java.net.UnknownHostException
import javax.inject.Inject

@ScreenScope
class SchoolViewModel @Inject constructor(
    private val api: SchoolApi,
) : ViewModel() {

    private var _schoolsData: MutableLiveData<BaseResponse<List<School>>>? = MutableLiveData()
    private val schoolsData: LiveData<BaseResponse<List<School>>>? get() = _schoolsData

    fun getAllSchools(): LiveData<BaseResponse<List<School>>> {
        _schoolsData = MutableLiveData()
        viewModelScope.launch {
            try {
                val data = api.getAllSchools()
                _schoolsData?.postValue(BaseResponse(data))
            } catch (e: UnknownHostException) {
                _schoolsData?.postValue(BaseResponse(null, e.message))
            }
        }
        return schoolsData!!
    }

    private var _schoolInfoData: MutableLiveData<BaseResponse<SchoolInfo>>? = MutableLiveData()
    private val schoolInfoData: LiveData<BaseResponse<SchoolInfo>>? get() = _schoolInfoData

    fun getSchoolInfo(dbn: String): LiveData<BaseResponse<SchoolInfo>> {
        _schoolInfoData = MutableLiveData()
        viewModelScope.launch {
            try {
                val data = api.getSchoolInfo(dbn)
                if (data.isNotEmpty()) {
                    _schoolInfoData?.postValue(BaseResponse(data[0]))
                } else {
                    _schoolInfoData?.postValue(BaseResponse(null, DATA_NOT_FOUND))
                }
            } catch (e: UnknownHostException) {
                _schoolInfoData?.postValue(BaseResponse(null, e.message))
            }
        }
        return schoolInfoData!!
    }
}